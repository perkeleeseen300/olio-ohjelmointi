/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vk3;


import java.util.Scanner;

/**
 *
 * @author Atte
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String name;
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Anna koiralle nimi: ");
        name = scan.nextLine();
        Dog d1 = new Dog(name);
        
        do {
            System.out.print("Mitä koira sanoo: ");
        } while(d1.speak(scan.nextLine()));
        
    }
}