/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk3;

import java.util.Scanner;

/**
 *
 * @author Atte
 */
public class Dog {
    private String name, says;
    
    public Dog(String s) {
        String temp;
        temp = s.trim();
        
        if (temp.isEmpty()) {
            name = "Doge";
        } else{
            name = s;
        }
        System.out.println("Hei, nimeni on " + name);
    }
        
     public boolean speak(String s) {
        boolean isEmpty;
        if(s.trim().isEmpty()) {
            says = "Much wow!";
            isEmpty = true;
        } else {
            says = s;
            isEmpty = false;
        }
        //System.out.println(name + ": " + says);
        Scanner scan = new Scanner(says);
        
        while (scan.hasNext()) {
            if (scan.hasNextInt()) {
                System.out.println("Such integer: " + scan.next());
            } else if (scan.hasNextBoolean()) {
                System.out.println("Such boolean: " + scan.next());
            } else {
                System.out.println(scan.next());
            }
        }
        
        return isEmpty;
    }
}