/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bottleDispenser;

/**
 *
 * @author Atte Putkonen 0418378
 */

import java.text.DecimalFormat;
import java.util.ArrayList;

public class BottleDispenser {
    
    static private BottleDispenser bd = null;
    private DecimalFormat df = new DecimalFormat("0.00");
    private int bottles;
    private float money;
    private ArrayList<Bottle> bottleArray = new ArrayList();

    private BottleDispenser() {
        bottles = 6;
        money = 0;
        bottleArray.add(new Bottle());
        bottleArray.add(new Bottle("Pepsi Max",1.5f,2.2f));
        bottleArray.add(new Bottle("Coca-Cola Zero",0.5f,2.0f));
        bottleArray.add(new Bottle("Coca-Cola Zero",1.5f,2.5f));
        bottleArray.add(new Bottle("Fanta Zero",0.5f,1.95f));
        bottleArray.add(new Bottle("Fanta Zero",0.5f,1.95f));
    }
    
    public Bottle getBottle(int index){
        return bd.bottleArray.get(index);
    }
    
    public String getMoney() {
        return df.format(money);
    }
    
    public static BottleDispenser getInstance() {
        if (bd == null)
            bd = new BottleDispenser();
        return bd;
    }
    
    public String addMoney(int addedMoney) {
        money += addedMoney;
        return ("Klink! Rahaa lisättiin " + df.format(addedMoney) + " €");
    }
    
    public String buyBottle(Bottle b) {
        String output;
        int index = bottleArray.indexOf(b) + 1;
        if (money < bottleArray.get(index-1).getPrize()) {
            output = ("Syötä rahaa ensin!");
        } else {
            if (bottles > 0) {
                bottles -= 1;
                money = money - bottleArray.get(index-1).getPrize();
                output = ("KACHUNK! "+bottleArray.get(index-1).getName()
                        +" tipahti masiinasta!");
                bottleArray.remove(index-1);
            } else {
                output = ("Valitsemasi tuote on loppunut!");
            }
        }
        return output;
    }

    public String returnMoney() {
        String output = ("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "
            + df.format(money) + "€");
        money = 0;
        return output;
    }
    
    public int getBottleAmount() {
        return bottles;
    }
}
