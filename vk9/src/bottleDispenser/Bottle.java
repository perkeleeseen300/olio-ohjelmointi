/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bottleDispenser;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class Bottle {
    private String name;
    private float size;
    private float prize;
    
    public Bottle() {
        name = "Pepsi Max";
        size = 0.5f;
        prize = 1.8f;
    }
    
    protected Bottle(String _name, float _size, float _prize){
        name = _name;
        size = _size;
        prize = _prize;
    }
    public String getName() {return name;}
    public float getSize() {return size;}
    public float getPrize() {return prize;}
    
    @Override
    public String toString() {
        String infoText;
        infoText = this.getName() + ", " + this.getSize() + "l, "
                + this.getPrize() + "€";
        return infoText;
    }
}
