/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk9;

import bottleDispenser.Bottle;
import bottleDispenser.BottleDispenser;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;


/**
 *
 * @author Atte Putkonen, 0418378
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button addMoney;
    @FXML
    private Button returnMoney;
    @FXML
    private Button buyButton;
    @FXML
    private ComboBox<Bottle> combo;
    @FXML
    private TextArea textField;
    @FXML
    private Slider addSlider;
    @FXML
    private Label moneyIndicator;
    @FXML
    private Button returnReceipt;
    
    private BottleDispenser bd = BottleDispenser.getInstance();
    private String bottleReceipt = "Et ole ostanut mitään!";
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        for (int i = 0; i < bd.getBottleAmount(); i++){
            combo.getItems().add(bd.getBottle(i));
        }
    }

    @FXML
    private void addMoneyAction(ActionEvent event) {
        textField.setText(bd.addMoney((int)addSlider.getValue())
                            + "\n" + textField.getText());
        moneyIndicator.setText("Rahaa koneessa: " + bd.getMoney() + " €");
    }

    @FXML
    private void returnMoneyAction(ActionEvent event) {
        textField.setText(bd.returnMoney() + "\n" + textField.getText());
        moneyIndicator.setText("Rahaa koneessa: 0 €");
    }

    @FXML
    private void buyButtonAction(ActionEvent event) {
        bottleReceipt = combo.getValue().toString();
        textField.setText(bd.buyBottle(combo.getValue()) + "\n" + textField.getText());
        combo.getItems().clear();
        for (int i = 0; i < bd.getBottleAmount(); i++){
            combo.getItems().add(bd.getBottle(i));
        }
        moneyIndicator.setText("Rahaa koneessa: " + bd.getMoney() + " €");
    }

    @FXML
    private void returnReceiptAction(ActionEvent event) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter("Kuitti.txt"));
        out.write("******* KUITTI *******");
        out.newLine();
        out.write(bottleReceipt);
        out.close();
    }
    
}
