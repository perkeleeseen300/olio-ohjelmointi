/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk4;

/**
 *
 * @author Atte Putkonen 0418378
 */
import java.text.DecimalFormat;
import java.util.ArrayList;

public class BottleDispenser {
    
    DecimalFormat df = new DecimalFormat("0.00");
    private int bottles;
    private float money;
    Bottle b = new Bottle();
    ArrayList<Bottle> bottleArray = new ArrayList();
    
    public BottleDispenser() {
        bottles = 6;
        money = 0;
        /*for(int i = 0; i < bottles; i++) {
            bottleArray.add(b);
        }*/
        bottleArray.add(b);
        bottleArray.add(new Bottle("Pepsi Max",1.5f,2.2f));
        bottleArray.add(new Bottle("Coca-Cola Zero",0.5f,2.0f));
        bottleArray.add(new Bottle("Coca-Cola Zero",1.5f,2.5f));
        bottleArray.add(new Bottle("Fanta Zero",0.5f,1.95f));
        bottleArray.add(new Bottle("Fanta Zero",0.5f,1.95f));
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int index) {
        if (money < bottleArray.get(index-1).getPrize()) {
            System.out.println("Syötä rahaa ensin!");
        } else {
            if (bottles > 0) {
                bottles -= 1;     
                money = money - bottleArray.get(index-1).getPrize();
                System.out.println("KACHUNK! "+bottleArray.get(index-1).getName()
                        +" tipahti masiinasta!");
                bottleArray.remove(index-1);
            } else {
                System.out.println("Pullot loppu!");
            }
        }
    }
    public void printBottles() {
        for(int i = 0; i < bottles; i++) {
            System.out.println(i+1 + ". Nimi: " + bottleArray.get(i).getName()
                + "\n\tKoko: " + bottleArray.get(i).getSize()
                + "\tHinta: " + bottleArray.get(i).getPrize());
        }
    }
    public void returnMoney() {
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "
            + df.format(money) + "€");
        money = 0;
    }
}
