/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk4;

/**
 *
 * @author Atte Putkonen, 0418378
 */
import java.util.Scanner;

public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int sel = -1;
        String temp;
        Scanner scan = new Scanner(System.in);
        BottleDispenser bd = new BottleDispenser();
        String menu = "\n*** LIMSA-AUTOMAATTI ***\n"+
                "1) Lisää rahaa koneeseen\n"+
                "2) Osta pullo\n"+
                "3) Ota rahat ulos\n"+
                "4) Listaa koneessa olevat pullot\n"+
                "0) Lopeta\nValintasi: ";
        
        OUTER:
        while (true) {
            System.out.print(menu);
            temp = scan.nextLine();
            sel = Integer.parseInt(temp);
            
            switch (sel) {
                case 1:
                    bd.addMoney();
                    break;
                case 2:
                    bd.printBottles();
                    System.out.print("Valintasi: ");
                    temp = scan.nextLine();
                    sel = Integer.parseInt(temp);
                    bd.buyBottle(sel);
                    break;
                case 3:
                    bd.returnMoney();
                    break;
                case 4:
                    bd.printBottles();
                    break;
                case 0:
                    break OUTER;
                default:
                    System.out.println("Virhe!");
                    break;
            }
        }
    }
}
