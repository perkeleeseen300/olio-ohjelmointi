/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk10_2;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Collections.sort;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class TheaterMap {
    private Document doc;
    
    ArrayList<String> showList = new ArrayList();
    
    private HashMap<String, String> map = new HashMap();
    Map<String, TheaterShows > theaterMap = new HashMap<>();
    private loadURL u = new loadURL();
    
    
    public HashMap<String, String> getMap() {
        return map;
    }

    public int getTheatersByMovie(String movie) {
        if (theaterMap.containsKey(movie))
            return theaterMap.get(movie).getTheaterList().size();
        return 0;
    }
  
    public String getTheater(String movieName, int index) {
        return theaterMap.get(movieName).getTheaterList().get(index);
    }
    
    public TheaterMap() {
        String content = u.loadURL("http://www.finnkino.fi/xml/TheatreAreas/");
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            parseMapData();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(TheaterMap.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public int listShows (String id, String date, String startTime, String endTime) {  // palauttaa elokuvien määrän
        showList.clear();
        String content = u.loadURL("http://www.finnkino.fi/xml/Schedule/?area="+id+"&dt="+date);
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            parseArrayData(startTime, endTime);
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(TheaterMap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return showList.size();
    }
    
    public void listTheaters (String date) {
        String content = u.loadURL("http://www.finnkino.fi/xml/Schedule/?area=1029"+"&dt="+date);
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            parseTheaterData();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(TheaterMap.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getShow(int index){
        return showList.get(index);
    }
    
    private void parseMapData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        ArrayList<String> ignoreList = new ArrayList(
            Arrays.asList("1014", "1012", "1002","1021"));
        
        for(int i = 1; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            if (!ignoreList.contains(getValue("ID",e)))
                map.put(getValue("Name",e), getValue("ID",e));
        }
    }

    private void parseTheaterData() {
        theaterMap.clear();
        NodeList nodes = doc.getElementsByTagName("Show");
        List<String> tempList = new ArrayList<String>();
        String tempString;
        for(int i = 1; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            tempString = getValue("Theatre",e) +"\t\t"+ convertTime(getValue("dttmShowStart",e));

            if (theaterMap.containsKey(getValue("Title",e))) {
                theaterMap.get(getValue("Title",e)).addItem(tempString);
            } else {
                theaterMap.put(getValue("Title",e), new TheaterShows(tempString));
            }
            tempList.clear();
        }
    }    
    
    private void parseArrayData(String startTime, String endTime) {
        NodeList nodes = doc.getElementsByTagName("Show");

        for(int i = 1; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            if ((startTime.length() == 5) & (endTime.length() == 5)) {
                if (!compareTime(startTime,endTime,(getValue("dttmShowStart",e)))){
                    continue;
                }
            } 
            showList.add((getValue("OriginalTitle",e)));
        }
    }
    
    public String convertTime (String startTime) {
        try {
            String refFormat = "yyyy-MM-dd'T'HH:mm:ss";
            String newFormat = "dd.MM.yyyy\tHH:mm";
            Date refDate = new SimpleDateFormat(refFormat).parse(startTime);
            String refDateString = new SimpleDateFormat(newFormat).format(refDate);     //tosi järkeviä converttailuja

            return refDateString;
            
         } catch (ParseException ex) {
            Logger.getLogger(TheaterMap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private boolean compareTime (String startTime, String endTime, String referenceTime){
        try {
            String refFormat = "yyyy-MM-dd'T'HH:mm:ss";
            String newFormat = "HH.mm";
            Date refDate = new SimpleDateFormat(refFormat).parse(referenceTime);
            String refDateString = new SimpleDateFormat(newFormat).format(refDate);     //tosi järkeviä converttailuja
            SimpleDateFormat formatter = new SimpleDateFormat(newFormat);
            
            Date startDate = formatter.parse(startTime);
            Date endDate = formatter.parse(endTime);
            Date reference = formatter.parse(refDateString);

            if ((startDate.before(reference)) & (endDate.after(reference))){
                return true;
            } else {
                return false;
            }
         } catch (ParseException ex) {
            Logger.getLogger(TheaterMap.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    private String getValue(String tag, Element e) {
        return (e.getElementsByTagName(tag)).item(0).getTextContent();
    }
    
    
}
class TheaterShows {
    private List<String> theaterList = new ArrayList<String>();
    public TheaterShows(String item){
        theaterList.add(item);
    }
    public void addItem (String item){
        theaterList.add(item);
        sort(theaterList);
    }
    public List<String> getTheaterList() {
        return theaterList;
    }
}
