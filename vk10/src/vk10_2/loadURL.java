/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk10_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class loadURL {
    public loadURL(){}
    
    public String loadURL(String _url) {//"http://www.finnkino.fi/xml/TheatreAreas/"
         BufferedReader br = null;
        try {
            URL url = new URL(_url);
            br = new BufferedReader(new InputStreamReader(url.openStream()));
            String content = "";
            String line;
            while ((line = br.readLine()) != null){
                content += line + "\n";
            }
            return content;
            
        } catch (IOException ex) {
            Logger.getLogger(loadURL.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(loadURL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

}
