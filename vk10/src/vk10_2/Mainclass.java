/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk10_2;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException {
        String oldFormat = "dd.mm.yyyy";
        String newFormat = "yyyy-mm-dd";
        
        String oldDate = "08.02.2016";
        
        SimpleDateFormat sdf = new SimpleDateFormat(oldFormat);
        Date d = sdf.parse(oldDate);
        sdf.applyPattern(newFormat);
        System.out.println(sdf.format(d));
        
  
    }
}
