/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk10;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import vk10_2.TheaterMap;
import vk10_2.loadURL;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label0;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Button listMoviesButton;
    @FXML
    private Button nameSearchButton;
    @FXML
    private ComboBox<String> combo;
    @FXML
    private TextField dateField;
    @FXML
    private TextField startTimeField;
    @FXML
    private TextField endTimeField;
    @FXML
    private TextField nameField;
    @FXML
    private TextArea textField;
    @FXML
    private ListView<String> listViewField;
    
    loadURL u = new loadURL();
    String content = u.loadURL("http://www.finnkino.fi/xml/TheatreAreas/");
    TheaterMap theaterMap = new TheaterMap();
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Object[] keys = theaterMap.getMap().keySet().toArray();
        Arrays.sort(keys);
        for (Object key : keys)
            combo.getItems().add(key.toString());
        
    }    

    @FXML
    private void listMoviesAction(ActionEvent event) {
        listViewField.getItems().clear();
        int showAmount = theaterMap.listShows(theaterMap.getMap().get(combo.getValue())
                , dateField.getText(), startTimeField.getText(),endTimeField.getText());
        for (int i = 0; i < showAmount; i++) {
            listViewField.getItems().add(theaterMap.getShow(i));
        }
    }

    @FXML
    private void nameSearchAction(ActionEvent event) {
        listViewField.getItems().clear();
        theaterMap.listTheaters(dateField.getText());
        
        if (!nameField.getText().isEmpty()) {
            if (theaterMap.getTheatersByMovie(nameField.getText()) != 0) {
                
                for (int i = 0; i < theaterMap.getTheatersByMovie(nameField.getText());i++) 
                    listViewField.getItems().add(theaterMap.getTheater(nameField.getText(),i));
            } else {
                listViewField.getItems().add("Elokuvia ei löytynyt");
            }
        } else {
                listViewField.getItems().add("Syötä elokuvan nimi");
        }
        
    }
}
