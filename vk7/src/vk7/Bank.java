/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk7;

import java.util.ArrayList;

/**
 *
 * @author Atte Putkonen, 0418378
 */


public class Bank {
    private ArrayList<Account> bank = new ArrayList();
    
    public Bank(){}
    
    /*public Bank(String acc, int bal, int cred){

        System.out.println("Pankkiin lisätään: " + account + "," + balance + "," + credit);
    }*/
    
    public void addAccount(Account a){
        bank.add(a);
    }
    public Account getAccount(String acc){
        for (int i = 0; i < bank.size(); i++){
            if (bank.get(i).getAccount().equals(acc)){
                return bank.get(i);
            }
        }
        return null;
    }
    
    public void remove(String acc){
        for (int i = 0; i < bank.size(); i++){
            if (bank.get(i).getAccount().equals(acc)){
                bank.remove(i);
            }
        }
        System.out.println("Tili poistettu.");
    }
    
    public void print(String acc){
        for (int i = 0; i < bank.size(); i++){
            if (bank.get(i).getAccount().equals(acc)){
                System.out.println("Tilinumero: " + acc + " Tilillä rahaa: " + 
                        bank.get(0).getBalance());
            }
        }
    }
    
    public void printAll(){
        System.out.println("Kaikki tilit:");
        for (int i = 0; i < bank.size(); i++){
            if (bank.get(i) instanceof ReqularAccount){
                System.out.println("Tilinumero: "+bank.get(i).getAccount()+
                        " Tilillä rahaa: "+bank.get(i).getBalance());
            } else if (bank.get(i) instanceof CreditAccount){
                System.out.println("Tilinumero: "+bank.get(i).getAccount()+
                        " Tilillä rahaa: "+bank.get(i).getBalance()+
                        " Luottoraja: "+bank.get(i).getCredit());
            }
        }
    }
}
