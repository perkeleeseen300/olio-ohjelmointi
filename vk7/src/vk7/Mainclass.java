/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk7;

import java.util.Scanner;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int sel;
        String account;
        int amount, credit;
        Scanner scan = new Scanner(System.in);
        Bank bank = new Bank();
        
        OUTER:
        while(true){
            System.out.print("\n*** PANKKIJÄRJESTELMÄ ***\n" +
                        "1) Lisää tavallinen tili\n" +
                        "2) Lisää luotollinen tili\n" +
                        "3) Tallenna tilille rahaa\n" +
                        "4) Nosta tililtä\n" +
                        "5) Poista tili\n" +
                        "6) Tulosta tili\n" +
                        "7) Tulosta kaikki tilit\n" +
                        "0) Lopeta\n" +
                        "Valintasi: ");
            
            sel = scan.nextInt();
            switch (sel){
                case 1:
                    System.out.print("Syötä tilinumero: ");
                    account = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    amount = scan.nextInt();
                    bank.addAccount(new ReqularAccount(account,amount));
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    account = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    amount = scan.nextInt();
                    System.out.print("Syötä luottoraja: ");
                    credit = scan.nextInt();
                    bank.addAccount(new CreditAccount(account,amount,credit));
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    account = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    amount = scan.nextInt();
                    bank.getAccount(account).deposit(amount);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    account = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    amount = scan.nextInt();
                    bank.getAccount(account).withdraw(amount);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    account = scan.next();
                    bank.remove(account);
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    account = scan.next();
                    bank.print(account);
                    break;
                case 7:
                    bank.printAll();
                    break;
                case 0:
                    break OUTER;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
            }
        }
    }
}
