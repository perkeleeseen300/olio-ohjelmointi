/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk7;

/**
 *
 * @author Atte Putkonen, 0418378
 */
abstract class Account {
    protected String account;
    protected int balance;
    protected int credit;
    
    public String getAccount() {
        return account;
    }
    
    public int getBalance() {
        return balance;
    }
    public int getCredit(){
        return credit;
    }
    public void deposit(int amount){
        balance = balance + amount;
    }
    
    public void withdraw(int amount){
        if (amount <= balance){
            balance = balance - amount;
        } else {
            System.out.println("Tilillä ei ole tarpeeksi rahaa.");
        }
        
    }
}

class ReqularAccount extends Account{
    public ReqularAccount(String a, int b){
        account = a;
        balance = b;
        System.out.println("Tili luotu.");
    }
}   
class CreditAccount extends Account{

    public CreditAccount(String a, int b, int c){
        account = a;
        balance = b;
        credit = c;
        System.out.println("Tili luotu.");
    }
    
    @Override
    public void withdraw(int amount){
        if (amount <= balance + credit){
            balance = balance - amount;
        } else {
            System.out.println("Luottoraja ei riitä.");
        }
    }
}


