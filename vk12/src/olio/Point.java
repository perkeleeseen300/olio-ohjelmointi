/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class Point {
    private double X;
    private double Y;
    private String name;
    private Circle c;
    
    
    public Point (double x, double y, String newName) {
        this.c = new Circle();
        X = x;
        Y = y;
        name = newName;
        
    }
    
    public Circle getCircle() {
        return c;
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public String getName() {
        return name;
    }
    
    
}
