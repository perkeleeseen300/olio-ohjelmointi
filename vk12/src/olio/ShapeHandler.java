/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio;

import java.util.ArrayList;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class ShapeHandler {
    private static final ShapeHandler instance = new ShapeHandler();
    private ArrayList<Point> points = new ArrayList<>();
    private ArrayList<Point> linePoints = new ArrayList<>(1);
    private boolean existVar = false;
    private static final Line line = new Line();
    
    private ShapeHandler(){}
    
    public static ShapeHandler getInstance() {
        return instance;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }
    
    public Point exist (Circle cir){
        for (Point p: points) {
            if (cir.getLayoutX() == p.getX() & cir.getLayoutY() == p.getY()) {
                existVar = true;
                return p;
            }
        }
        return null;
    }
    public Line choosePoint (Point p) {
        if (linePoints.size() == 1) {
            Line l = drawLine(linePoints.get(0),p);
            linePoints.set(0, p);
            return l;
        } else if (linePoints.isEmpty()) {
            linePoints.add(p);
        }
        return null;
    }
    
    private Line drawLine(Point p1, Point p2) {
        line.setStartX(p1.getX());
        line.setStartY(p1.getY());
        line.setEndX(p2.getX());
        line.setEndY(p2.getY());
        return line;
    }

    public boolean isExistVar() {
        return existVar;
    }

    public void resetExistVar() {
        this.existVar = false;
    }
    
    
}
