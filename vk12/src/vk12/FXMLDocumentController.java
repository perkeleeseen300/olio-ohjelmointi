/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk12;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import olio.Point;
import olio.ShapeHandler;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane base;
    
    private Line drawLine = new Line();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

  @FXML
    private void mouseClickAction(MouseEvent event) {
        if (!ShapeHandler.getInstance().isExistVar()){
            int count = ShapeHandler.getInstance().getPoints().size();
            Point p = new Point(event.getX(),event.getY(),"Piste"+count);
            Circle c = p.getCircle();
            c.setLayoutX(p.getX());
            c.setLayoutY(p.getY());
            c.setRadius(5);
            c.setFill(Color.BLUE);

            c.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    Point p = ShapeHandler.getInstance().exist(c);
                    System.out.println("Hei, olen "+ p.getName() +"!");
                    
                    Line l = ShapeHandler.getInstance().choosePoint(p);
                    if (l != null) {
                        
                        drawLine.setStartX(l.getStartX());
                        drawLine.setStartY(l.getStartY());
                        drawLine.setEndX(l.getEndX());
                        drawLine.setEndY(l.getEndY());
                        drawLine.setStroke(Color.RED);
                        System.out.println(drawLine);
                        if (!base.getChildren().contains(drawLine))
                            base.getChildren().add(drawLine);
                    }
                }
            });

            base.getChildren().add(c);
            ShapeHandler.getInstance().getPoints().add(p);
            System.out.println(p.getName() + " x: " + p.getX() + " y: " + p.getY());
        }
        ShapeHandler.getInstance().resetExistVar();
    }
}