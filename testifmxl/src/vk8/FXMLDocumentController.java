/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;


/**
 *
 * @author Atte Putkonen, 0418378
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private Label label;
    @FXML
    private TextField inputField;
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    @FXML
    private TextField saveFileName;
    @FXML
    private TextField loadFileName;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void printAction(KeyEvent event) {
        label.setText(inputField.getText());
    }

    @FXML
    private void saveButtonAction(ActionEvent event) {
        saveFileName.setVisible(true);
        label.setText(label.getText() + "\n\nSyötä tiedostonimi ja paina Enter.");
    }

    @FXML
    private void loadButtonAction(ActionEvent event) {
        loadFileName.setVisible(true);
        label.setText("Syötä tiedostonimi ja paina Enter.");
    }

    @FXML
    private void saveToFile(ActionEvent event) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(saveFileName.getText()));
        out.write(label.getText());
        out.close();
        label.setText("Teksti tallennettu tiedostoon " + saveFileName.getText());
        inputField.setText("");
        saveFileName.setText("");
        saveFileName.setVisible(false);
    }

    @FXML
    private void loadFromFile(ActionEvent event) throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(loadFileName.getText()));
        label.setText(in.readLine());
        in.close();
        inputField.setText("");
        loadFileName.setText("");
        loadFileName.setVisible(false);
    }

    @FXML
    private void clearLabel(MouseEvent event) {
        label.setText("");
    }

}
