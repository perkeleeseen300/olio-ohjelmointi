/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class ReadAndWriteIO {
    //private String filename;
    
    public ReadAndWriteIO(){}

    public void readFile(String inFilename) throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(inFilename));
        String inputLine;
        while((inputLine = in.readLine()) != null) {
            System.out.append(inputLine+"\n");
        }
        in.close();
    }
    public void readAndWrite(String inFilename,String outFilename) throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(inFilename));
        BufferedWriter out = new BufferedWriter(new FileWriter(outFilename));
        String inputLine;
        while((inputLine = in.readLine()) != null) {
            if((inputLine.length() < 30) && inputLine.replaceAll(" ", "").length() > 0) {
                if(inputLine.indexOf("p") != -1){
                    out.write(inputLine);
                    out.newLine();
                }
            }
        }
        in.close();
        out.close();
    }
    public void readZip(String filename) throws IOException {
        String inputLine;
        ZipFile zip = new ZipFile(filename);
        
        for(Enumeration e = zip.entries(); e.hasMoreElements();){
            ZipEntry entry = (ZipEntry) e.nextElement();
            
            BufferedReader br = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
            while((inputLine = br.readLine()) != null) {
                System.out.println(inputLine);
            }
        }
    }
}
