/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk6;

import java.util.Scanner;

/**
 *
 * @author j9922
 */
class Character {
    
    int v = 5;
    protected String characterName;
    protected String weaponName;

    
    Scanner scan = new Scanner(System.in);
    
    public Character() {
    }
    
    public void chooseCharacter() {
        System.out.print("Valitse hahmosi: \n1) Kuningas\n2) Ritari\n"
            +"3) Kuningatar\n4) Peikko\nValintasi: ");
        v = scan.nextInt();
        switch (v) {
            case 1:
                new King();
                break;
            case 2:
                new Knight();
                break;
            case 3:
                new Queen();
                break;
            case 4:
                new Troll();
                break;
                
        }   
    }
    public void chooseWeapon() {
        Weapon weapon = new Weapon();
        System.out.print("Valitse aseesi:\n1) Veitsi\n2) Kirves\n"
            +"3) Miekka\n4) Nuija\nValintasi: ");
        v = scan.nextInt();
        
        switch (v) {
            case 1:
                weaponName = weapon.returnWeapon(1);
                break;
            case 2:
                weaponName = weapon.returnWeapon(2);
                break;
            case 3:
                weaponName = weapon.returnWeapon(3);
                break;
            case 4:
                weaponName = weapon.returnWeapon(4);
                break;
        }
    }


        
        
            
    

    public void fight() {
        System.out.println(characterName +" taistelee aseella " +weaponName);
    }
       
    class King {
        public King() {
            characterName = "King";
        }
    }
    class Knight {
        public Knight() {
            characterName = "Knight";
        }
    }
    class Troll {
        public Troll() {
            characterName = "Troll";
        }
    }
    class Queen {
        public Queen() {
            characterName = "Queen";
        }
    }
}
    
class Weapon {
    protected String weaponName;
    public Weapon(){
    }
   
    public String returnWeapon(int v) {
        switch(v) {
            case 1:
                weaponName = "Knife";
                break;
            case 2:
                weaponName = "Axe";
                break;
            case 3:
                weaponName = "Sword";
                break;
            case 4:
                weaponName = "Club";
                break;
        }
        return weaponName;
    }
    
    class Knife {
        public Knife() {
            
        }
    }
    class Axe {
        public Axe() {
            
        }
    }
    class Sword {
        public Sword() {
            
        }
    }
    class Club {
        public Club() {
            weaponName = "Club";
        }
    }
    

 
}
    
    

