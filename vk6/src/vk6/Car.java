/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk6;

/**
 *
 * @author Atte Putkonen, 0418378
 */
class Car {
   /* private Body body;
    private Chassis chassis;
    private Engine engine;
    private Wheel wheel1;
    private Wheel wheel2;
    private Wheel wheel3;
    private Wheel wheel4;
    */
    public Car() {
        Body body = new Body();
        Chassis chassis = new Chassis();
        Engine engine = new Engine();
        Wheel wheel1 = new Wheel();
        Wheel wheel2 = new Wheel();
        Wheel wheel3 = new Wheel();
        Wheel wheel4 = new Wheel();

    }
    public void print() {
        System.out.println("Autoon kuuluu:");
        System.out.println("\tBody");
        System.out.println("\tChassis");
        System.out.println("\tEngine");
        System.out.println("\t4 Wheel");
    }
}



class Body {
    public Body() {
        System.out.println("Valmistetaan: Body");
    }
}
class Chassis {
    public Chassis() {
        System.out.println("Valmistetaan: Chassis");
    }
}
class Engine {
    public Engine() {
        System.out.println("Valmistetaan: Engine");
    }
}
class Wheel {
    public Wheel() {
        System.out.println("Valmistetaan: Wheel");
    }
}
    
