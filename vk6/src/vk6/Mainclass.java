/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk6;

import java.util.Scanner;

/**
 *
 * @author Atte Putkonen, 0418378
 */

public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*Car car = new Car();
        car.print();*/
        
        
        int v = 1;
        int v2 = 1;
        String characterName;
        String weapon;
        Character character = new Character();
        Scanner scan = new Scanner(System.in);
        while (v != 0) {
            System.out.print("*** TAISTELUSIMULAATTORI ***\n"
                        +"1) Luo hahmo\n2) Taistele hahmolla\n0) Lopeta\nValintasi: ");
            v = scan.nextInt();
            switch(v) {
                case 1:
                    character.chooseCharacter();
                    character.chooseWeapon();
                    break;
                case 2:
                    character.fight();
                    break;
                case 0:
                    
                    break;
                default:
                    System.out.println("Virhe");
                    break;
            }
        }
    }
    
}
