/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vk11;

import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author Atte Putkonen, 0418378
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private WebView web;
    @FXML
    private TextField searchPanel;
    @FXML
    private Button loadButton;
    @FXML
    private Button refreshButton;
    
    private ArrayList<String> previousList = new ArrayList<>(10);
    private ArrayList<String> nextList = new ArrayList<>(10);
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load("http://www.google.fi/?gfe_rd=cr&ei=YSJMWIqBPNCq8weg2ZzACw");
        
    }    

    @FXML
    private void loadAction(ActionEvent event) {
        String webSite = searchPanel.getText();
        if (!webSite.isEmpty()){
            previousList.add(web.getEngine().getLocation());
            if (webSite.equals("index.html")){
                web.getEngine().load(getClass().getResource("index.html").toExternalForm());        
            } else {
                if (!webSite.contains("http://")) {
                    web.getEngine().load("http://" + webSite);
                } else {
                    web.getEngine().load(webSite);
                }
            }
            String currentSite = web.getEngine().getLocation();
            if (currentSite.equals(previousList.get(previousList.size()-1))){
                previousList.remove(previousList.size()-1);
            }
            searchPanel.setText(currentSite);
            listLimiter(previousList);
        }
    }

    @FXML
    private void refreshAction(ActionEvent event) {
        web.getEngine().reload();
    }

    @FXML
    private void shoutOutAction(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void initializeAction(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }
    
    @FXML
    private void previousAction(ActionEvent event) {
        if (!previousList.isEmpty()){
            nextList.add(web.getEngine().getLocation());
            String webSite = iterator(previousList);
            if (webSite.contains("/vk11/index.html")){
                web.getEngine().load(getClass().getResource("index.html").toExternalForm());        
            } else {
                if (!webSite.contains("http://") & !webSite.contains("https://")) {
                    web.getEngine().load("http://" + webSite);
                } else {
                    web.getEngine().load(webSite);
                }
            }
            searchPanel.setText(web.getEngine().getLocation());
            previousList.remove(previousList.size()-1);
        }
    }

    @FXML
    private void nextAction(ActionEvent event) {
        if (!nextList.isEmpty()){
            previousList.add(web.getEngine().getLocation());
            String webSite = iterator(nextList);
            if (webSite.equals("index.html")){
                web.getEngine().load(getClass().getResource("index.html").toExternalForm());        
            } else {
                if (!webSite.contains("http://") & !webSite.contains("https://")) {
                    web.getEngine().load("http://" + webSite);
                } else {
                    web.getEngine().load(webSite);
                }
            }
            searchPanel.setText(web.getEngine().getLocation());
            nextList.remove(nextList.size()-1);
        }
    }
    
    private String iterator(ArrayList<String> al) {
        ListIterator iter = al.listIterator(al.size());
        if (iter.hasPrevious()) {
            return (String)iter.previous();
        }
        return web.getEngine().getLocation();
    }

    @FXML
    private void isChanged(MouseEvent event) {
        previousList.add(web.getEngine().getLocation());
    }

    @FXML
    private void getSite(MouseEvent event) {
        String currentSite = web.getEngine().getLocation();
        if (currentSite.equals(previousList.get(previousList.size()-1))) {
            previousList.remove(previousList.size()-1);
        } else {
            searchPanel.setText(currentSite);
            listLimiter(previousList);
        }
    }

    @FXML
    private void selectAll(MouseEvent event) {
        searchPanel.selectAll();
    }
    
    private void listLimiter(ArrayList<String> al) {
        int size = al.size();
        if (size > 10) {
           al.remove(0);
        }
    }
}
